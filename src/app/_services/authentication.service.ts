import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient) { }

  login(name: string, password: string) {
    return this.http.post<any>('/login', { name: name, password: password })
        .pipe(map(user => {
            if (user) {
                localStorage.setItem('currentUser', JSON.stringify(user));
            }

            return user;
        }));
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

}
