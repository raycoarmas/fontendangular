import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_services/authentication.service';
import { ValidationService } from '../_services/validation.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  loginForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) {
    this.loginForm = this.formBuilder.group({
      'name': ['', Validators.required],
      'password': ['', [Validators.required, ValidationService.passwordValidator]]
    });
  }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.submitted = false;
      this.loading = false;
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.loginForm.value.name, this.loginForm.value.password)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['usertable']);
        },
        error => {
          this.alertService.error(error.error.message);
          this.loading = false;
      });
  }

}
