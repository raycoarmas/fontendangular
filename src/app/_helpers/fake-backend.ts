import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../_model/User';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  constructor() {
    localStorage.setItem('users', JSON.stringify(ELEMENT_DATA));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const users: User[] = JSON.parse(localStorage.getItem('users')) || [];

    return of(null).pipe(mergeMap(() => {

      // authenticate
      if (request.url.endsWith('/login') && request.method === 'POST') {
        const filteredUsers = users.filter(user => {
          return user.name === request.body.name && user.password === request.body.password;
        });

        if (filteredUsers.length) {
          const user = filteredUsers[0];
          const body = {
            id: user.id,
            name: user.name,
            password: user.password,
            roles: user.roles
          };

          return of(new HttpResponse({ status: 200, body: body }));
        } else {
          return throwError({ error: { message: 'Username or password is incorrect' } });
        }
      }

      // get users
      if (request.url.endsWith('/users') && request.method === 'GET') {
        return of(new HttpResponse({ status: 200, body: users }));

      }

      // register user
      if (request.url.endsWith('/createuser') && request.method === 'POST') {
        const user: User = JSON.parse(localStorage.getItem('currentUser'));
        if (user === null || !user.roles.includes('admin')) {
          return throwError({ error: { message: 'Must be logged as admin' } });
        }
        const newUser = request.body;

        newUser.id = users.length + 1;
        users.push(newUser);
        localStorage.setItem('users', JSON.stringify(users));

        return of(new HttpResponse({ status: 200 }));
      }
      return next.handle(request);

    }))

      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());
  }
}

export let fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};

const ELEMENT_DATA: User[] = [
  { id: 1, name: 'Felipe', roles: ['standard', 'admin'], password: '123456' },
  { id: 2, name: 'Roberto', roles: ['standard'], password: '123456' },
  { id: 3, name: 'Maria', roles: ['standard'], password: '123456' },
  { id: 4, name: 'Rebeca', roles: ['admin'], password: '123456' }
];
