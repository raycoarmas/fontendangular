import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserTableComponent } from './user-table/user-table.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path : '',
    component : LoginComponent
  },
  {
    path : 'usertable',
    component : UserTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
