import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { User } from '../_model/User';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../_services/validation.service';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../_services/alert.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html'
})
export class UserTableComponent implements OnInit {

  currentUser: User;
  userForm: FormGroup;
  loading = false;
  submitted = false;
  roles: string[] = [];
  id = 4;
  displayedColumns: string[] = ['id', 'name', 'roles'];
  dataSource = new MatTableDataSource();

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router) {
    this.userForm = this.formBuilder.group({
      'name': ['', Validators.required],
      'password': ['', [Validators.required, ValidationService.passwordValidator]],
      'admin': ['', ''],
      'standard': ['', '']
    });

  }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.loadAllUsers();
  }

  newUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    }

    this.loading = true;

    if (this.userForm.value.admin === true) {
      this.roles.push('admin');
    }
    if (this.userForm.value.standard === true || this.userForm.value.admin === '' || this.userForm.value.admin === null) {
      this.roles.push('standard');
    }

    this.userService.createUser(new User(null, this.userForm.value.name, this.userForm.value.password, this.roles))
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', true);
        },
        error => {
          this.loading = false;
          this.router.navigate(['']);
          this.alertService.error(error.error.message);
        });

    this.loadAllUsers();

    this.roles = [];
    this.loading = false;
    this.submitted = false;
    this.userForm.reset();
  }

  private loadAllUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe(
        data => {
          this.dataSource.data = data;
        });
  }
}
